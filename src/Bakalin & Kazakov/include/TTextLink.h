﻿#pragma once

#include <iostream>
#include <fstream>
#include <string>

#include "TDatValue.h"

#define MEMORY_SIZE 20

class TText;
class TTextLink;

class TTextMem {
public:
    TTextLink* pFirst; // указатель на первое звено
    TTextLink* pLast;  // указатель на последнее звено
    TTextLink* pFree;  // указатель на первое свободное звено

    friend class TTextLink;
};

class TTextLink : public TDatValue {
public:
    // Методы для управления памятью
    static void InitMemSystem(size_t size = MEMORY_SIZE); // инициализация памяти
    static void PrintFreeLink(void);     // печать свободных звеньев
    static void MemCleaner(TText& text); // сборка мусора
    void* operator new(size_t size);     // выделение звена
    void operator delete(void* pM);      // освобождение звена

    // Методы для работы со звеном текста
    TTextLink(std::string s = "", TTextLink* pn = nullptr, TTextLink* pd = nullptr);
    ~TTextLink(void) {}
    int IsAtom(void); // проверка атомарности звена
    TTextLink* GetNext(void);
    TTextLink* GetDown(void);
    TDatValue* GetCopy(void);

protected:
    std::string str_; // поле для хранения строки текста
    TTextLink *pNext_, *pDown_; // указатели по тек. уровень и на подуровень
    static TTextMem memHeader_; // система управления памятью

    friend std::ostream& operator<<(std::ostream &os, TTextLink &tm);
    friend class TText;
};
