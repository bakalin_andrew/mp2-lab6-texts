#include "TText.h"

static int textLevel; // номер текущего уровня текста

TText::TText(TTextLink* pl) {
    if (pl == nullptr)
        pl = new TTextLink();
    pFirst_ = pl;
    GoFirstLink();
}

TText::~TText() {
    pFirst_ = nullptr;
}

//
// Копирование текста
//

// Поиск первого атома
TTextLink* TText::GetFirstAtom(TTextLink* pl) {
    TTextLink* tmp = pl;

    while (!tmp->IsAtom()) {
        st_.push(tmp);
        tmp = tmp->GetDown();
    }

    return tmp;
}

TText* TText::GetCopy() {
    TTextLink *pl1, *pl2, *pl = pFirst_, *cpl = nullptr;

    if (pFirst_ != nullptr) {
        while (!st_.empty())
            st_.pop();

        while (true) {
            if (pl != nullptr) {
                pl = GetFirstAtom(pl);
                st_.push(pl);
                pl = pl->GetDown();
            } else if (st_.empty()) {
                break;
            } else {
                pl1 = st_.top();
                st_.pop();

                if (pl1->str_ != "Copy") {
                    pl2 = new TTextLink("Copy", pl1, cpl);
                    st_.push(pl2);
                    pl = pl1->GetNext();
                    cpl = nullptr;
                } else {
                    pl1->str_ = pl1->pNext_->str_;
                    pl1->pNext_ = cpl;
                    cpl = pl1;
                }
            }
        }
    }

    return new TText(cpl);
}

//
// Навигация
//

// Переход к первой строке
void TText::GoFirstLink() {
    while (!path_.empty())
        path_.pop();
    pCurrent_ = pFirst_;
}

// Переход к следующей строке по Down
void TText::GoDownLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pDown_;
        }
}

// Переход к следующей строке по Next
void TText::GoNextLink() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            path_.push(pCurrent_);
            pCurrent_ = pCurrent_->pNext_;
        }
}

// Переход к предыдущей позиции в тексте
void TText::GoPrevLink() {
    if (!path_.empty()) {
        pCurrent_ = path_.top();
        path_.pop();
    }
}

//
// Доступ
//

// Чтение текущей строки
std::string TText::GetLine() {
    if (pCurrent_ == nullptr)
        return "";
    else
        return pCurrent_->str_;

}

// Замена текущей строки
void TText::SetLine(std::string s) {
    if (pCurrent_ != nullptr)
        pCurrent_->str_ = s;
}

//
// Модификация
//

// Вставка строки в подуровень
void TText::InsDownLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, pd, nullptr);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка раздела в подуровень
void TText::InsDownSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pd = pCurrent_->pDown_;
        TTextLink* pl = new TTextLink(s, nullptr, pd);
        pCurrent_->pDown_ = pl;
    }
}

// Вставка строки в том же уровне
void TText::InsNextLine(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, pn, nullptr);
        pCurrent_->pNext_ = pl;
    }
}

// Вставка раздела в том же уровне
void TText::InsNextSection(std::string s) {
    if (pCurrent_ != nullptr) {
        TTextLink* pn = pCurrent_->pNext_;
        TTextLink* pl = new TTextLink(s, nullptr, pn);
        pCurrent_->pNext_ = pl;
    }
}

// Удаление строки в подуровне
void TText::DelDownLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pDown_ = pl2;
        }
}

// Удаление раздела в подуровне
void TText::DelDownSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pDown_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pDown_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pDown_ = pl2;
        }
}

// Удаление строки в том же уровне
void TText::DelNextLine() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            if (pl1->pDown_ == nullptr)
                pCurrent_->pNext_ = pl2;
        }
}

// Удаление раздела в том же уровне
void TText::DelNextSection() {
    if (pCurrent_ != nullptr)
        if (pCurrent_->pNext_ != nullptr) {
            TTextLink* pl1 = pCurrent_->pNext_;
            TTextLink* pl2 = pl1->pNext_;
            pCurrent_->pNext_ = pl2;
        }
}

//
// Итератор
//

// Установить на первую запись
void TText::Reset() {
    while (!st_.empty())
        st_.pop();

    pCurrent_ = pFirst_;
    if (pCurrent_ != nullptr) {
        st_.push(pCurrent_);
        if (pCurrent_->pNext_ != nullptr)
            st_.push(pCurrent_->pNext_);
        if (pCurrent_->pDown_ != nullptr)
            st_.push(pCurrent_->pDown_);
    }
}

// Текст завершён?
bool TText::IsTextEnded() const {
    return !st_.size();
}

// Переход к следующей записи
void TText::GoNext() {
    if (!IsTextEnded()) {
        pCurrent_ = st_.top();
        st_.pop();
        if (pCurrent_ != pFirst_) {
            if (pCurrent_->pNext_ != nullptr)
                st_.push(pCurrent_->pNext_);
            if (pCurrent_->pDown_ != nullptr)
                st_.push(pCurrent_->pDown_);
        }
    }
}

//
// Работа с файлами
//

// Ввод текста из файла
void TText::Read(char* pFileName) {
    std::ifstream txtFile(pFileName);

    if (txtFile)
        pFirst_ = ReadText(txtFile);
}

// Вывод текста в файл
void TText::Write(char* pFileName) {
    textLevel = 0;
    std::ofstream textFile(pFileName);

    PrintTextF(pFirst_, textFile);
}

//
// Печать
//

// Печать текста
void TText::Print() {
    textLevel = 0;
    PrintText(pFirst_);
}

// Печать текста со звена ptl
void TText::PrintText(TTextLink* ptl) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            std::cout << "  ";
        std::cout << " " << *ptl << std::endl;
        textLevel++;
        PrintText(ptl->GetDown());
        textLevel--;
        PrintText(ptl->GetNext());
    }
}

// Печать текста со звена ptl в файл
void TText::PrintTextF(TTextLink *ptl, std::ofstream& textFile) {
    if (ptl != nullptr) {
        for (int i = 0; i < textLevel; i++)
            textFile << ' ';
        textFile << ' ' << ptl->str_ << std::endl;
        textLevel++;
        PrintTextF(ptl->GetDown(), textFile);
        textLevel--;
        PrintTextF(ptl->GetNext(), textFile);
    }
}

// Чтение текста из файла
TTextLink* TText::ReadText(std::ifstream& txtFile) {
    std::string buf;
    TTextLink *ptl, *pHead;
    pHead = ptl = new TTextLink();

    while (!txtFile.eof()) {
        getline(txtFile, buf);

        if (buf[0] == '}')
            break;
        else if (buf[0] == '{')
            ptl->pDown_ = ReadText(txtFile);
        else {
            ptl->pNext_ = new TTextLink(buf);
            ptl = ptl->pNext_;
        }
    }
    ptl = pHead;
    if (pHead->pDown_ == nullptr) {
        pHead = pHead->pNext_;
        delete ptl;
    }
    return pHead;
}
